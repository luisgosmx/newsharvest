# Harvester

## Configuration
Before you start working you needed:
- git client
- maven 
- jdk 1.8
- A Java IDE of your choice (not mandatory)
- chromedriver
- Selenium

## The tests were run in the following environment

- Hardware: MacBook Pro (2.6 Ghz Intel Core I7, 16GB DDR3).
- SO: Mac OS Big Sur
- Google Chrome 86
- Safari 14.1

_execution times could change_

## Setup
In order to get the code:

- Create a folder (Windows): 
```
md d:\HarvesterNews
cd d:\HarvesterNews
git clone https://gitlab.com/luisgosmx/newsharvest.git
cd newsharvest
```
- Compile everything
```
mvn clean install
```

## Run the harvest

### NEWS

 **command**
 
 `java -jar harvester.jar -newsharvest -namesource XXXX -output XXXX`
 

**Ways to get information**
 - **RSS** (Really Simple Syndication) XML format to distribute content on the web.
 - **Web scraping** Extract information from websites using the HTTP protocol.

**newspapers available**

 - La Vanguardia (España) https://www.lavanguardia.com ****_RSS_**** `-namesource lavanguardia`
 - El País (España) https://elpais.com/espana/ ****_RSS_****`-namesource elpais`
 - Reforma (México)  https://www.reforma.com   **_RSS_**            `-namesource reforma`
 - El Comercio (Perú) https://elcomercio.pe    **_RSS_**          `-namesource elcomercio`
 - La República (Perú) https://larepublica.pe   **_RSS_**          `-namesource larepublica`
 - El Observador (Uruguay)  https://www.elobservador.com.uy   **_RSS_**     `-namesource elobservador`
 - El tiempo (Colombia) https://www.eltiempo.com/    **_RSS_**        `-namesource eltiempo`
 - Listín Diario (R. Dominicana)  https://listindiario.com  **_RSS_** `-namesource listindiario`
 - Clarín (Argentina) https://www.clarin.com    **_RSS_**          `-namesource clarin`
 - La Nación (Argentina) https://elnacional.com.do    **_Web scraping_**     `-namesource lanacion`
 - Emol (Chile) https://www.emol.com     **_Web scraping_**          `-namesource emol` 
 - El Universal (México) https://www.eluniversal.com.mx   **_Web scraping_**       `-namesource eluniversal`
 - La Tercera (Chile) https://www.latercera.com   **_Web scraping_**         `-namesource latercera`
 - El País (Uruguay) https://www.elpais.com.uy    **_Web scraping_**          `-namesource elpaisuy`
 - El espectador (Colombia) https://www.elespectador.com  **_Web scraping_**     `-namesource elespectador`
 - La Nación (Argentina) https://www.lanacion.com.ar    **_Web scraping_**     `-namesource lanacionarg`
 - Las últimas noticias (Venezuela) https://ultimasnoticias.com.ve **_Web scraping_**`-namesource ultimasnoticiasve`
 - El Universal (Venezuela) https://www.eluniversal.com **_Web scraping_**      `-namesource eluniversalven`
 - **ALL** `-namesource all`

**Example result**
- Newspaper: La Vanguardia (España) - https://www.lavanguardia.com
- URL news: https://www.lavanguardia.com/vida/20201030/4974106905/el-coronavirus-es-mas-contagioso-ahora-que-cuando-llego-el-pasado-invierno.html



**The following example shows the final result in json format**

_has the following properties_
- Keywords (about the news)
- Publication date  (date of publication of the news)
- Source (url)
- Id (automatically generated)
- News title
- News text
- News category
- Date the news was extracted
```
{
  "news": {
    "keywords": "última hora, últimas noticias, noticias en espanol, al minuto, entretenimiento, videos, Catalunya, Cataluña, España, deportes, economía, internacional, internet, noticias, opinión, periódico, política, Barcelona, comunidad, tecnología, vanguardia, sucesos, gente, mundo, información, participación",
    "publication_date": "Fri, 30 Oct 2020 06:00:00 +0100",
    "id": "VAN3010202011341",
    "source": "https:\/\/www.lavanguardia.com\/vida\/20201030\/4974106905\/el-coronavirus-es-mas-contagioso-ahora-que-cuando-llego-el-pasado-invierno.html",
    "text": "El virus de la Covid-19 es ahora más contagioso que cuando llegó el pasado invierno pero no se ha vuelto más virulento, según coinciden en señalar distintos estudios que han analizado cómo ha evolucionado el SARS-CoV-2 desde su aparición. El patógeno ha adquirido la capacidad de transmitirse con más eficiencia gracias a una mutación genética que le ha permitido modificar la proteína S (del inglés Spike, o espiga). Esta es la proteína que el virus utiliza a modo de gancho para fijarse a la membrana de las células humanas y poder infectarlas. La mutación, llamada D614G, le permite agarrarse a las células con más eficiencia, según una investigación de la Universidad de Massachusetts presentada en el servidor bioRxiv . Nuevos datos presentados esta semana en la revista Nature muestran que la mutación acelera la proliferación del virus en las vías respiratorias altas pero no en los pulmones. La multiplicación acelerada del virus explica que las cepas dotadas de esta mutación sean ahora hegemónicas en el mundo. Dado que las personas infectadas con estas cepas producen y emiten mayor cantidad de virus, pueden contagiar más fácilmente a otras per- sonas. Los nuevos datos explican también que el coronavirus no haya evolucionado hacia una mayor virulencia, ya que la mutación D614G no eleva el riesgo de afectar a los pulmones y causar neumonía. Rápido aumento de casos La mayor transmisión es uno de los factores que pueden agravar la segunda ola en Europa Los coronavirus que circulan ahora descienden de una cepa identificada por primera vez en una muestra de un paciente de Italia del 20 de febrero. En aquel momento no se sabía aún que esta cepa se convertiría en dominante. Fueron científicos de EE.UU. quienes reconstruyeron su historia en una investigación publicada el 20 de agosto en la revista Cell . Demostraron que los virus con la mutación D614G se extendieron rápidamente a toda Europa y al resto del mundo. A finales de marzo la mutación ya se encontraba en el 60% de todas las secuencias del SARS-CoV-2 analizadas a escala global. En mayo, ya estaba en el 90%. Y a finales de junio, casi en el 100%. Pero quedaba la duda de cuál era el motivo de esta rápida expansión. Podía ser que la mutación fuera beneficiosa para el virus y le diera una ventaja competitiva respecto a otras cepas, que serían eliminadas por selección natural. O bien podía intervenir lo que en biología se llama el efecto fundador, según el cual las características genéticas de una población (en este caso, una población de virus) descienden de los primeros individuos que la fundaron. Dos nuevas investigaciones presentadas esta semana sustentan la primera hipótesis. En la revista Nature , científicos de la Universidad de Texas (EE.UU.) han publicado los resultados de experimentos en que han comparado la actividad de virus con la mutación D614G y de virus sin la mutación. En células y en tejidos del aparato respiratorio humano, han observado que los virus SARS-CoV-2 que tienen la mutación producen hasta 2,4 veces más de nuevos virus que aquellos que no la tienen. Selección natural El SARS-CoV-2 tiene ahora más capacidad de proliferar en las vías respiratorias altas En experimentos con hámsters, la carga viral en las vías respiratorias altas ha aumentado mucho más en los animales infectados con virus mutados. En los pulmones, por el contrario, no se ha observado una diferencia significativa en la carga viral. Tampoco se ha visto que los animales perdieran más o menos peso según el tipo de virus que se les inoculó, lo que sugiere que la mutación no agrava los síntomas de la Covid-19. Los investigadores han realizado un experimento adicional en que han infectado a animales con las dos cepas del virus al mismo tiempo. Rápidamente, los virus mutados se han convertido en mayoritarios y los no mutados se han visto arrinconados. Incluso cuando el 90% de los virus que se han inoculado no tenían la mutación, el 10% que sí la tenía ha proliferado más rápidamente y se ha convertido en dominante desde el primer día. En otra investigación presentada en el servidor bioRxiv , un equipo internacional de científicos de Suiza, Alemania y Estados Unidos ha confirmado que los virus SARS-CoV-2 dotados de la mutación D614G se fijan con más eficiencia a las células humanas y proliferan más en células de las vías respiratorias altas. Los investigadores han analizado la actividad del virus en hámsters, ratones y hurones y han comprobado cómo la mutación aumenta la capacidad del virus de multiplicarse y de transmitirse a otros animales en las tres especies. \u201CHemos demostrado que [la mutación] D614G aumenta la replicación viral en las vías respiratorias superiores. Este hallazgo tiene implicaciones importantes para comprender la evolución de la pandemia de Covid-19\u201D, escriben los investigadores de la Universidad de Texas en Nature. La incidencia del coronavirus en Catalunya (Diseño LV) Extensión global Las variantes del virus con la mutación D614G son ahora hegemónicas en el mundo La mayor eficiencia del coronavirus para propagarse puede explicar en parte el rápido aumento de casos registrado en Europa en las últimas semanas. Pero \u201Cintervienen también otros factores\u201D, advierte Daniel Prieto-Alhambra, fármaco-epidemiólogo de la Universidad de Oxford (Reino Unido). Entre ellos destaca \u201Cel regreso de la movilidad después del verano, el regreso al trabajo y la socialización en espacios interiores\u201D. Otro factor que ha influido, añade el virólogo Adolfo García-Sastre, del hospital Mount Sinai de Nueva York, es el rastreo insuficiente de contactos que se ha dado en España y en gran parte de Europa. Al principio \u201Cla curva empieza a subir sin que uno se entere mucho, porque casi no sube\u201D, explica García-Sastre. \u201CPero llega un momento en que ha subido tanto que el diagnóstico y el rastreo son insuficientes, y entonces la curva se dispara, porque los casos empiezan a multiplicarse a una velocidad logarítmica\u201D. ¿La segunda ola del coronavirus es peor que la primera? Sí No La encuesta está cerrada Votar Total votos: 0",
    "title": "El coronavirus es más contagioso ahora que cuando llegó el pasado invierno",
    "category": "Vida",
    "date_harvest": "30-10-2020T11:3:41"
  }
}
```


## DEFAULTER

#### sources available

- Sanciones Consolidadas Mexico https://sanciones.cnbv.gob.mx `-namesource sancionesmx`

 **command**
 
 `java -jar harvester.jar -defaulterharvest -name "XXXX" -namesource XXXX -output XXXX`
 

**Example result**

- source: sancionesmx
- name: Banco Compartamos, S.A.

`java -jar harvester.jar -defaulterharvest -name "Banco Compartamos, S.A." -namesource sancionesmx -output defaulter`

**name of the output file:** SAN20112020171431.json


```
[
  {
    "ClavePes": 0,
    "Infractor": "BANCO COMPARTAMOS, S.A., INSTITUCIÓN DE BANCA MÚLTIPLE",
    "TipoSancion": "Amonestación",
    "SectorId": 0,
    "Sector": "Instituciones de banca múltiple",
    "Monto": 0.00,
    "ConductaInfringida": "LIC - No atender requerimientos de información dentro del plazo",
    "LeyInfringida": "LIC - Ley de Instituciones de Crédito",
    "LeyesInfringidas": [
      {
        "NombreCorto": "LIC",
        "NombreCompleto": "Ley de Instituciones de Crédito",
        "Conducta": "No atender requerimientos de información dentro del plazo",
        "ArticuloSancion": "108, fracción II, inciso h)",
        "ArticuloRegulador": "97"
      }
    ],
    "FechaImposicion": "\/Date(1490162400000)\/",
    "FechaImposicionString": "22/03/2017",
    "PeriodoPublicacion": "201704",
    "Historico": false,
    "SanctionOriginToPublish": 0,
    "Id": 42557,
    "CustomProperties": {}
  },
  {
  .
  .
  }
]
```
**_Execution time: 5.0 seconds._**

#### sources available

- Office of Foreign Assets Control https://sanctionssearch.ofac.treas.gov  `-namesource sanctions`
- Resource available for the following countries: **Jamaica, México, Brasil y Dominicana**

**command**
 
 `java -jar harvester.jar -defaulterharvest -name "XXXX" -namesource XXXX -output XXXX`

 **Example result**

- source:  Office of Foreign Assets Control
- name: INMOBILIARIA FER CADENA

`java -jar harvester.jar -defaulterharvest -name "INMOBILIARIA FER CADENA" -namesource sanctions -output defaulter`

**name of the output file:** SAS2311202085521.json

```
{
  "defaulter": {
    "address": "Diagonal 23 #11-07 P.2",
    "name": "INMOBILIARIA FER CADENA",
    "id": "SAS2311202085521",
    "source": "https:\/\/sanctionssearch.ofac.treas.gov\/Details.aspx?id=3891",
    "program": "SDNTK",
    "type": "Entity",
    "list": "SDN"
  }
}
```
**_Execution time: 3.0 seconds._**

#### sources available 

- companies office of Jamaica https://www.orcjamaica.com/CompanySearchResults.aspx `-namesource orcjamaica`
- Resource available for the following countries: **Jamaica**

**command**
 
 `java -jar harvester.jar -defaulterharvest -name "XXXX" -namesource orcjamaica -os XXXX -output XXXX`
 

 **Example result**

- source:  orcjamaica
- name: Jamaica

- `-os Mac or Windows`

`java -jar harvester.jar -defaulterharvest -name "Jamaica" -namesource orcjamaica -os Mac -output defaulter`

**name of the output file:** ORJ23112020132148.json

200 results
```
{
  "link": "",
  "rank": 0,
  "id": "57125-L",
  "name": "Jamaica  All  Natural Limited",
  "office": "",
  "status": "ACTIVE",
  "type": "COMPANY",
  "date": "24 Apr 1997",
  "address": "Address is Not Available.",
  "number": "57125 (Local)",
  "documentCount": "22"
},
{
.
.
.
}
```
**_Execution time: 53.0 seconds._**

#### sources available 

- Superintendencia del mercado de valores https://www.smv.gob.pe/Frm_Sanciones.aspx `-namesource smv`
- Resource available for the following countries: **Peru**

**command**
 
 `java -jar harvester.jar -defaulterharvest -date dd/mm/yyyy -namesource XXXX -os XXXX -output XXXX`
 

 **Example result**

- source:  mvn
- `-os Mac or Windows`
- `-date format dd/mm/yyy` *Date available from 01/01/2008.

`java -jar harvester.jar -defaulterharvest -date 10/10/2018 -namesource smv -os Mac -output defaulter`

**name of the output file:** SMV23112020191438.json

206 results
```
{
    "defaulter": {
        "amount": "12,450.00",
        "sumilla": "Se sanciona a AC Capitales Sociedad Administradora de Fondos de Inversión S.A. con una multa de 3 UIT equivalente a S\/ 12 450,00 (doce mil cuatrocientos cincuenta y 00\/100 soles) por haber incurrido en una (1) infracción de naturaleza muy grave, tipificada en el Anexo I, numeral 1, inciso 1.3 del Reglamento de Sanciones, al revelar información protegida con el deber de reserva de identidad establecido en el artículo 45° de la Ley del Mercado de Valores",
        "name": "AC CAPITALES SOCIEDAD ADMINISTRADORA DE FONDOS DE INVERSION S.A.",
        "id": "SMV23112020191438",
        "type": "Multa",
        "resolution number": "RSASP N° 038-2020",
        "resolution date": "14\/10\/2020",
        "with recourse": "No"
    }
},
{
.
.
.
}
```
**_Execution time: 220.0 seconds._**


#### sources available 

- INDECOPI https://servicio.indecopi.gob.pe/appCPCBuscador/ `-namesource indecopi`
- Resource available for the following countries: **Peru**

**command**
 
 `java -jar harvester.jar -defaulterharvest -name "XXXX" -namesource XXXX -os XXXX -output XXXX`
 

 **Example result**

- name "CMAC TACNA S.A."
- source  indecopi
- `-os Mac or Windows`


`java -jar harvester.jar -defaulterharvest -name "CMAC TACNA S.A." -namesource indecopi -os Mac -output defaulter`

**name of the output file:** IDC2911202022738.json

2 results
```
{
    "defaulter": {
        "records": [
            {
                "date": "2019",
                "fine": "2019-03-21",
                "matter": "CREDITO DE CONSUMO",
                "offending fact": "FALTA DE IDONEIDAD",
                "resolution": "151-2019\/CPC-INDECOPI-AQP",
                "resolution date": "2019-03-21"
            },
            {
                "date": "2018",
                "fine": "2018-12-19",
                "matter": "PRESTAMO PERSONAL",
                "offending fact": "FALTA DE IDONEIDAD",
                "resolution": "3573-2018\/SPC",
                "resolution date": "2018-12-19"
            },
            {
                "date": "2018",
                "fine": "2018-12-19",
                "matter": "PRESTAMO PERSONAL",
                "offending fact": "INFORMACION - INCUMPLIMIENTO DE LA OBLIGACION DE INFORMAR",
                "resolution": "3573-2018\/SPC",
                "resolution date": "2018-12-19"
            },
            {
                "date": "2018",
                "fine": "2018-07-23",
                "matter": "PRESTAMO PERSONAL",
                "offending fact": "FALTA DE IDONEIDAD",
                "resolution": "1828-2018\/SPC",
                "resolution date": "2018-07-23"
            },
            {
                "date": "2018",
                "fine": "2018-01-08",
                "matter": "PRESTAMO PERSONAL",
                "offending fact": "METODOS ABUSIVOS DE COBRANZA - OTROS",
                "resolution": "20-2018\/CPC-INDECOPI-AQP",
                "resolution date": "2018-01-08"
            },
            {
                "date": "2017",
                "fine": "2017-06-28",
                "matter": "PRESTAMO PERSONAL",
                "offending fact": "LIBRO DE RECLAMACIONES - HOJAS DEL LIBRO DE RECLAMACIONES NO CUMPLEN CON LAS ESPECIFICACIONES DEL REGLAMENTO",
                "resolution": "2115-2017\/SPC",
                "resolution date": "2017-06-28"
            },
            {
                "date": "2017",
                "fine": "2017-06-28",
                "matter": "PRESTAMO PERSONAL",
                "offending fact": "LIBRO DE RECLAMACIONES - OTROS",
                "resolution": "2115-2017\/SPC",
                "resolution date": "2017-06-28"
            }
        ],
        "name": "CAJA MUNICIPAL DE AHORRO Y CRÉDITO DE TACNA S.A.",
        "total amount": "19.55 UIT",
        "id": "IDC2911202022738",
        "comercial name": "CMAC TACNA S.A.",
        "total sanctions": "8"
    }
},
{
.
.
.
}
```
**_Execution time: 16.0 seconds._**

#### sources available 

- SBS GOB https://www.sbs.gob.pe/usuarios/informacion-financiera/informalidad-financiera/entidades-informales-identificadas-por-la-sbs `-namesource sbsgob`
- Resource available for the following countries: **Peru**

**command**
 
 `java -jar harvester.jar -defaulterharvest -namesource XXXX -output XXXX`
 

 **Example result**

- source  sbsgob


`java -jar harvester.jar -defaulterharvest -namesource sbsgob -output defaulter`

**name of the output file:** SBS112202012142.json

21 results
```
{
    "defaulter": {
        "modality": "Oferta de gran rentabilidad por la entrega de dinero",
        "entities": "Banco Omega Pro\nAirbit Club\nIM Mastery Academy\nQuantico\nFinancika\nInnnova International Tech\n",
        "id": "SBS112202012142",
        "source": "https:\/\/www.sbs.gob.pe\/noticia\/detallenoticia\/idnoticia\/2489",
        "text": "Lima, 09 de junio de 2020.- La Superintendencia de Banca, Seguros y AFP (SBS) advierte a la ciudadanía que se han detectado nuevos esquemas de negocio que ofrecen altas ganancias por su dinero, los que no cuentan con autorización de la SBS para captar dinero del público, conforme lo establece el artículo 11 de la Ley 26702, Ley General del Sistema Financiero y del Sistema de Seguros. Empresas detectadas bajo el esquema que ofrecen gran rentabilidad por la entrega de dinero: Banco Omega Pro Airbit Club IM Mastery Academy Quantico Financika Innova International Tech Empresas detectadas bajo el esquema de préstamos fraudulentos: Coopesacc Prestasol Crediconfiable En ese sentido, la SBS advierte que cualquier ofrecimiento público sobre esquemas de negocios que ofrecen altas ganancias a cambio de su dinero, puede constituir un esquema piramidal o una estafa, por lo que deben tomar las precauciones del caso. Especialmente, la SBS advierte sobre las siguientes modalidades de captación ilegal de ahorros: Ofrecimiento de negocios o inversiones que, en poco tiempo, generan altas y seguras ganancias. Esquemas de supuesta educación financiera que permiten convertirse en expertos, para invertir en operaciones de cambio de monedas (Forex) o en mercados bursátiles, y ganar mucho dinero en poco tiempo. Sistemas que ofrecen bonos o ganancias por invitar a más personas a comprar paquetes o membresías de negocios supuestamente rentables (pirámides). Ante ello, la SBS invocó a los ciudadanos a informarse adecuadamente y a tomar las previsiones del caso, cuando deba decidir dónde ahorrar o invertir su dinero, sobre todo en estos momentos de Estado de Emergencia Nacional en que las personas retiran parte de su fondo de pensiones o son beneficiadas por los bonos que otorga en Gobierno. Cualquier consulta o denuncia relacionada con estos esquemas de negocio no autorizados, o esquemas similares, puede ser presentada a la SBS, comunicándose a los teléfonos 0-800-10840 (línea gratuita a nivel nacional) o al 01-200-1930, o escribir al correo electrónico informalidad@sbs.gob.pe. También pueden visitar nuestra página www.sbs.gob.pe\/informalidad en donde podrá encontrar la relación de entidades autorizadas a captar dinero del público, así como las entidades, empresas o esquemas de negocios que han sido detectadas por ofrecer servicios financieros sin la debida autorización. A TENER EN CUENTA La SBS se preocupa de combatir la informalidad financiera e informalidad de seguros con la finalidad de proteger al público usuario de posibles estafas, siendo la más común de estas la pirámide financiera. Para ello monitorea de manera constante las diversas regiones del país con el objetivo de poder detectar a empresas que incumpliendo el marco normativo, se dediquen a realizar actividades de intermediación financiera y de seguros sin autorización de la SBS. Para evitar ser víctima de estos delitos es necesario que se tenga en cuenta lo siguiente: Captar dinero del público sin autorización de la SBS es un delito. Antes de depositar tu dinero verifica que sea en entidades financieras supervisadas por la SBS. Revisa aquí la relación de empresas autorizadas a captar depósitos. Desconfía de propuestas que te ofrecen obtener grandes ganancias en poco tiempo a cambio de entregar tu dinero. No invites a más personas a participar. En la modalidad de pirámide financiera, estas empresas informales te piden incluir a más personas a cambio de mayores ganancias. No te dejes convencer por conocidos que afirman estar recibiendo grandes ganancias. Muchas veces estas empresas informales pagan lo prometido al inicio buscando acumular una buena cantidad de participantes, para luego desaparecer con todo el dinero captado. Si conoces casos de informalidad financiera denúncialos escribiendo a: informalidad@sbs.gob.pe",
        "title": "SBS advierte a la ciudadanía no dejarse sorprender por entidades informales que ofrecen grandes ganancias por su dinero"
    }
}
```
**_Execution time: 30.0 seconds._**







