package harvest.maching;

import com.google.gson.JsonArray;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

/**
 *
 * @author golivares
 */
public class Maching {

    public static String EXPORT_PATH = "";
    public static int con = 0;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        EXPORT_PATH = "news/";
        publishJSONnews();
        //readfolder();
//        searchElasticSearch();
//        ArrayList<String> word = new ArrayList<String>();
//        word.add("estafadores usan mensajes de correo");
//        word.add("informacion");
//        String text = "Los estafadores usan mensajes de correo electrónico o mensajes de texto para engañarlo y lograr que usted les dé su información personal. Pueden tratar de robarle sus contraseñas, números de cuenta o números de Seguro Social. Si consiguen esa información, podrían acceder a su cuenta de email, banco u otras cuentas. Todos los días, los estafadores lanzan miles de ataques de phishing como estos, y suelen tener éxito.";
//        taggedtext(word, text);

    }

    //the function of only reading files in a folder
    public static void readfolder() throws FileNotFoundException, IOException {
        int contador = 0;
        ArrayList words = readfile();
        File carpeta = new File(EXPORT_PATH);
        String[] listado = carpeta.list();
        if (listado == null || listado.length == 0) {
            System.out.println("There are no items inside the folder");
        } else {
            for (String listado1 : listado) {
                if (listado1.contains(".json")) {
                    BufferedReader bf = new BufferedReader(new FileReader(EXPORT_PATH + listado1));
                    String sCadena;
                    while ((sCadena = bf.readLine()) != null) {
                        try {
                            JsonParser parser = new JsonParser();
                            FileReader fr = new FileReader(EXPORT_PATH + listado1);

                            JsonElement datos = parser.parse(fr);
                            JsonObject json = datos.getAsJsonObject();
                            JsonObject jsonshema = json.get("news").getAsJsonObject();

                            if (jsonshema.has("text")) {
                                String text = jsonshema.get("text").getAsString();
                                if (text.length() > 0) {
                                    maching(text.toLowerCase(), listado1);
                                    taggedtext(words, text);
                                    //searchElasticSearch(text.toLowerCase(), listado1);
                                }
                            }
                        } catch (Exception e) {
                        }
                    }
                }
            }
        }
    }

    //does a simple word comparison
    public static void maching(String text, String namefile) {
        ArrayList words = readfile();
        for (int i = 0; i < words.size(); i++) {
            String word = words.get(i).toString();
            if (word.toLowerCase().contains(text)) {
                System.out.println("el archivo " + namefile + " contiene " + word + " texto " + text);
            }
        }
    }

    //does a search in elasticsearch to see if it compares with the text of the news
    public static void searchElasticSearch() throws IOException {
        ArrayList words = readfile();
        int cont = 0;
        for (int x = 0; x < words.size(); x++) {
            String word = words.get(x).toString();
            String id = null;
            String score = null;
            int icurrent = 0;
            int irowcount = 100;

            String searchFrase = word;//request.getParameter("searchPhrase");
            String scollection = "textnews";

            //query on elasticsearc
            HttpClient httpClient = HttpClientBuilder.create().build();
            String query = "{\n"
                    + "  \"from\" : " + icurrent + ", \"size\" : " + irowcount + ",\n"
                    + "  \"query\": {\n"
                    + "    \"bool\":  {\"should\": [\n"
                    + "          { \"match\":{\"text\":\"" + searchFrase + "\"}}\n"
                    + "      ]\n"
                    + "    }\n"
                    + "  }\n"
                    + "}";
            String url = "http://localhost:9200/" + scollection + "/doc/_search";
            HttpPost post = new HttpPost(url);
            StringEntity postingString = new StringEntity(query, "UTF-8");
            post.setEntity(postingString);
            post.setHeader("Content-type", "application/json");
            HttpResponse response1 = httpClient.execute(post);
            HttpEntity entity = response1.getEntity();
            String body = EntityUtils.toString(entity, "UTF-8");
            JsonObject jobj = new JsonParser().parse(body).getAsJsonObject();
            JsonObject jh = jobj.get("hits").getAsJsonObject();
            JsonElement tel = jh.get("total");
            int tot = 0;
            if (tel.isJsonObject()) {
                tot = tel.getAsJsonObject().get("value").getAsInt();
            } else {
                tot = tel.getAsInt();
            }
            if (tot > 0) {
                JsonArray ja = jh.get("hits").getAsJsonArray();
                int tam = ja.size();
                for (int i = 0; i < tam; i++) {
                    JsonObject jo = ja.get(i).getAsJsonObject();
                    if (jo != null) {
                        id = jo.get("_id").getAsString();
                        score = jo.get("_score").toString();
                        float newscore = Float.parseFloat(score);
                        if (newscore > 5) {
                            System.out.println(cont + " la palabra " + word + " esta contenida en el id " + id + " con un score de " + score);
                            cont++;
                        }
                    }
                }
            }
        }
    }

    //just read the keywords file
    public static ArrayList readfile() {
        ArrayList words = new ArrayList();
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;
        try {
            archivo = new File("src/main/resource/importantwords.txt");
            fr = new FileReader(archivo);
            br = new BufferedReader(fr);
            String linea;
            while ((linea = br.readLine()) != null) {
                words.add(linea.trim());
            }
        } catch (IOException e) {
        }
        return words;
    }

    //send a json file and save it in elasticsearch
    public static void publishJSONnews() throws IOException {
        File carpeta = new File(EXPORT_PATH);
        String[] listado = carpeta.list();
        if (listado == null || listado.length == 0) {
            System.out.println("There are no items inside the folder");
        } else {
            for (String listado1 : listado) {
                if (listado1.contains(".json")) {
                    BufferedReader bf = new BufferedReader(new FileReader(EXPORT_PATH + listado1));
                    String sCadena;
                    while ((sCadena = bf.readLine()) != null) {

                        try {
                            JsonParser parser = new JsonParser();
                            FileReader fr = new FileReader(EXPORT_PATH + listado1);
                            JsonElement datos = parser.parse(fr);
                            JsonObject json = datos.getAsJsonObject();
                            JsonObject jsonshema = json.get("news").getAsJsonObject();

                            if (jsonshema.has("text")) {
                                String text = jsonshema.get("text").getAsString();
                                String date = jsonshema.get("publication_date").getAsString();
                                date = date.replaceAll("(/)", "-");
                                jsonshema.addProperty("publication_date", date);
                                if (text.length() > 0) {
                                    CloseableHttpClient httpClient = HttpClients.createDefault();
                                    try {
                                        String q = "http://localhost:9200/textnews/doc/" + listado1.replaceAll(".json", "");
                                        HttpPost post = new HttpPost(q);
                                        StringEntity postingString = new StringEntity(jsonshema.toString(), "UTF-8");
                                        post.setEntity(postingString);
                                        post.setHeader("Content-type", "application/json; charset=utf-8");
                                        CloseableHttpResponse response = httpClient.execute(post);
                                        response.close();
                                    } finally {
                                        httpClient.close();
                                    }
                                    System.out.println(" Save " + con + " " + listado1.replaceAll(".json", ""));
                                    con++;
                                }
                            }
                        } catch (Exception e) {
                        }
                    }
                }
            }
        }
    }

    //This function is used to label words it works in the following way receives
    //a text and separates it by spaces after the words or phrases it separates them
    //in the same way then it compares word by word if it finds one it saves it if the next
    //one is the same it keeps it if it meets With all the sentence given, it saves it in the
    //new text already labeled, otherwise it discards it and continues with the next word or phrase.
    public static void taggedtext(ArrayList words, String text) {
        String palabras = "";
        ArrayList<String> word = new ArrayList<String>(words);
        boolean contein = false;
//        byte[] ptext = text.getBytes(ISO_8859_1);
//        text = new String(ptext, UTF_8);
        String[] parts = text.split(" ");

        String textnew = "";
        int contadorveces = 0;
        List<String> stopword = Arrays.asList("");

        String minitext = "";
        for (int i = 0; i < parts.length; i++) {
            String palabra = parts[i];
            if (contadorveces == 0) {

                for (int j = 0; j < word.size(); j++) {
                    String key = word.get(j).toLowerCase();
//                    System.out.println("key = " + key);
                    String[] keyparts = key.split(" ");
                    String newpalabra = palabra;

                    int newcont = i;
                    String contained = null;

                    for (int k = 0; k < keyparts.length; k++) {
                        String keytermpart = keyparts[k];
                        for (int w = 0; w < stopword.size(); w++) {
                            String stop = stopword.get(w);
                            if (stop.equals(keytermpart)) {
                                contained = "si";
                                break;
                            }
                            contained = "no";
                        }

                        if (contained.equals("no")) {
                            for (int w = 0; w < stopword.size(); w++) {
                                String stop = stopword.get(w);
                                if (stop.equals(newpalabra.toLowerCase())) {
                                    minitext += newpalabra + " ";
                                    contained = "si";
                                    newcont = newcont + 1;
                                    if (parts.length > newcont) {
                                        newpalabra = parts[newcont];
                                        contadorveces = contadorveces + 1;
                                    }
                                    k--;
                                    break;
                                }
                                contained = "no";
                            }
                            if (contained.equals("no")) {
                                String original = newpalabra;
                                String cadenaNormalize = Normalizer.normalize(original, Normalizer.Form.NFD);
                                String cadenaSinAcentos = cadenaNormalize.replaceAll("[^\\p{ASCII}]", "");
                                String palabralimpia = cadenaSinAcentos.replaceAll("\\.", "").replaceAll(",", "").toLowerCase();
                                if (palabralimpia.contains(keytermpart)) {
                                    minitext += newpalabra + " ";
                                    newcont = newcont + 1;
                                    if (parts.length > newcont) {
                                        newpalabra = parts[newcont];
                                        contadorveces = contadorveces + 1;
                                    } else {
                                        minitext = "";
                                        break;
                                    }
                                } else {
                                    minitext = "";
                                    contadorveces = 0;
                                    break;
                                }
                            }
                        }

                    }

                    if (minitext.length() > 0) {
                        int entrar = 0;
                        minitext = minitext.substring(0, minitext.length() - 1);
                        if (entrar == 0) {
                            contein = true;
                            textnew += "<SEARCH>" + minitext + "</SEARCH> ";
                            palabras = minitext + "\n";
                            break;
                        } else {
                            break;
                        }
                    }
                }
                if (contadorveces == 0) {
                    textnew += palabra;
                    textnew += " ";

                } else {
                    contadorveces = contadorveces - 1;
                }
            } else {
                contadorveces = contadorveces - 1;
            }
        }
        if (contein == true) {
//            System.out.println(textnew + "\n\n");
//            System.out.println("");
//            System.out.println("");

            BufferedWriter bw = null;
            FileWriter fw = null;

            try {
                String data = palabras + "\n" + textnew + "\n";
                palabras = "";
                File file = new File("archivo.txt");
                // Si el archivo no existe, se crea!
                if (!file.exists()) {
                    file.createNewFile();
                }
                // flag true, indica adjuntar información al archivo.
                fw = new FileWriter(file.getAbsoluteFile(), true);
                bw = new BufferedWriter(fw);
                bw.write(data + "\n");
                System.out.println("información agregada!");
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    //Cierra instancias de FileWriter y BufferedWriter
                    if (bw != null) {
                        bw.close();
                    }
                    if (fw != null) {
                        fw.close();
                    }
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
}
