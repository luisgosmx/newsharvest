package harvest.maching;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author golivares
 */
public class Fuction {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        ArrayList<String> valuelist = new ArrayList<String>();
        valuelist.add("canciones");
        valuelist.add("casas");
        valuelist.add("camarones");
        valuelist.add("casita");
        valuelist.add("botella");
        valuelist.add("camaron");
        valuelist.add("botellas");
        quit_plural(valuelist);
    }

    //the purpose of this function is to remove plurals with drama rules
    public static void quit_plural(ArrayList<String> valuelist) {
        int cont = 0;
        for (int i = 0; i < valuelist.size(); i++) {
            int ind = i;
            String termword = valuelist.get(i);
            String term = termword.replace(",", "").replace("-", "");
            String plu = "";
            if ("es".equals(term.substring(term.length() - 2)) || "s".equals(term.substring(term.length() - 1))) {
                String[] slp = term.split(" ");
                for (int j = 0; j < slp.length; j++) {
                    String partj = slp[j];

                    if ("es".equals(partj.substring(partj.length() - 2)) && !"t".equals(partj.substring(partj.length() - 3, partj.length() - 2)) && !"l".equals(partj.substring(partj.length() - 3, partj.length() - 2)) || "les".equals(partj.substring(partj.length() - 3))) {
                        plu += " " + partj.substring(0, partj.length() - 2);

                        if ("on".equals(plu.substring(plu.length() - 2))) {
                            plu = " " + plu.substring(0, plu.length() - 2) + "ón";
                        }
                        if ("v".equals(plu.substring(plu.length() - 1))) {
                            plu = " " + plu + "e";
                        }
                        if ("bl".equals(plu.substring(plu.length() - 2))) {
                            plu = " " + plu + "e";
                        }
                        if ("br".equals(plu.substring(plu.length() - 2))) {
                            plu = " " + plu + "e";
                        }
                    } else if ("s".equals(partj.substring(partj.length() - 1))) {
                        plu += " " + partj.substring(0, partj.length() - 1);
                        int pos = j;
                        if (pos > 0) {
                            String bef = slp[0];
                            if ("n".equals(bef.substring(bef.length() - 1)) && !"ón".equals(bef.substring(bef.length() - 2))) {
                                String[] spblp = plu.split(" ");
                                String first = spblp[1];
                                if (!"n".equals(first.substring(first.length() - 1))) {
                                } else {
                                    String plu0 = first.substring(-1, 0);

                                    String join1 = plu = plu0 + " ";
                                }
                            }
                        }
                    } else {
                        plu += " " + partj;
                    }
                }
                ind = valuelist.indexOf(term);
                valuelist.set(ind, plu);
                cont = cont + 1;
            }
        }
        ArrayList<String> quit_plu = new ArrayList<String>();
        Collections.sort(valuelist);

        ArrayList<String> deletes = new ArrayList<String>();
        ArrayList<String> newl = new ArrayList<String>();
        for (int i = 0; i < valuelist.size(); i++) {
            if (newl.contains(valuelist.get(i))) {
                deletes.add(valuelist.get(i));
            } else {
                newl.add(valuelist.get(i));
            }
        }
        System.out.println(valuelist);
    }
}
