package harvest.types;

import harvest.save.SaveJson;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.tongfei.progressbar.*;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.apache.commons.io.FileUtils;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

/**
 *
 * @author golivares
 */
public class NewsHarvester {

    public static String EXPORT_PATH = "";

    /**
     * @param args the command line arguments
     * @throws java.lang.InterruptedException
     */
    public static void main(String[] args) throws InterruptedException {
        EXPORT_PATH = "test2/";
        elcomercio();
    }

    public static void select(String source, String folder) throws InterruptedException {

        if (!folder.endsWith("/")) {
            folder = folder + "/";
            EXPORT_PATH = folder;
        }
        EXPORT_PATH = folder;

        String validate = "no";
        if (source.equals("lavanguardia")) {
            validate = "si";
            lavanguardia();
        }
        if (source.equals("elpais")) {
            validate = "si";
            elpais();
        }
        if (source.equals("reforma")) {
            validate = "si";
            reforma();
        }
        if (source.equals("elcomercio")) {
            validate = "si";
            elcomercio();
        }
        if (source.equals("larepublica")) {
            validate = "si";
            larepublica();
        }
        if (source.equals("elobservador")) {
            validate = "si";
            elobservador();
        }
        if (source.equals("eltiempo")) {
            validate = "si";
            eltiempo();
        }
        if (source.equals("listindiario")) {
            validate = "si";
            listindiario();
        }
        if (source.equals("clarin")) {
            validate = "si";
            clarin();
        }
        if (source.equals("lanacion")) {
            validate = "si";
            lanacion();
        }
        if (source.equals("emol")) {
            validate = "si";
            Emol();
        }
        if (source.equals("eluniversal")) {
            validate = "si";
            eluniversal();
        }
        if (source.equals("latercera")) {
            validate = "si";
            latercera();
        }
        if (source.equals("elpaisuy")) {
            validate = "si";
            elpaisuy();
        }
        if (source.equals("elespectador")) {
            validate = "si";
            elespectador();
        }
        if (source.equals("lanacionarg")) {
            validate = "si";
            lanacionarg();
        }
        if (source.equals("ultimasnoticiasve")) {
            validate = "si";
            ultimasnoticiasve();
        }
        if (source.equals("eluniversalven")) {
            validate = "si";
            eluniversalven();
        }
        if (source.equals("all")) {
            validate = "si";
            lavanguardia();
            elpais();
            reforma();
            elcomercio();
            larepublica();
            elobservador();
            eltiempo();
            listindiario();
            clarin();
            lanacion();
            Emol();
            eluniversal();
            latercera();
            elpaisuy();
            elespectador();
            lanacionarg();
            ultimasnoticiasve();
            eluniversalven();
        }
        if (validate.equals("no")) {
            System.out.println("Invalid newspaper");
        }

    }

    // La Vanguardia (España)     https://www.lavanguardia.com/mvc/feed/rss/home
    //extract the information using the http protocol and export it to json format
    public static void lavanguardia() throws InterruptedException {
        String url = "https://www.lavanguardia.com/mvc/feed/rss/home";
        try {
            org.jsoup.nodes.Document doc = Jsoup.connect(url).userAgent("Mozilla/5.0").timeout(2000000000).get();
            org.jsoup.select.Elements lista = doc.getElementsByTag("item");
            ProgressBar lvProgressBar = new ProgressBar("La Vanguardia (España)", lista.size());
            for (int i = 0; i < lista.size(); i++) {

                Calendar fecha = new GregorianCalendar();
                int año = fecha.get(Calendar.YEAR);
                int mes = fecha.get(Calendar.MONTH);
                int dia = fecha.get(Calendar.DAY_OF_MONTH);
                int hora = fecha.get(Calendar.HOUR_OF_DAY);
                int minuto = fecha.get(Calendar.MINUTE);
                int segundo = fecha.get(Calendar.SECOND);

                SaveJson docjson = new SaveJson();
                Element noticia = lista.get(i);
                docjson.setDate_harvested(dia + "-" + (mes + 1) + "-" + año + "T" + hora + ":" + minuto + ":" + segundo);
                String title = noticia.getElementsByTag("title").text();
                docjson.setTitle(title);
                String link = noticia.getElementsByTag("link").text();
                docjson.setSource(link);
                String id = "VAN" + dia + (mes + 1) + año + hora + minuto + segundo;
                docjson.setId(id);

                String pubDate = noticia.getElementsByTag("pubDate").text();
                docjson.setPublication_date(pubDate);
                String category = noticia.getElementsByTag("category").text();
                docjson.setCategory(category);
                try {

                    org.jsoup.nodes.Document dochtml = Jsoup.connect(link).userAgent("Mozilla/5.0").timeout(2000000000).get();
                    org.jsoup.select.Elements meta = dochtml.getElementsByTag("meta");
                    for (int j = 0; j < meta.size(); j++) {
                        Element name = meta.get(j);
                        String nam = name.attr("name");
                        if (nam.equals("Keywords")) {
                            String val = name.attr("content");
                            docjson.setKeywords(val);
                            break;
                        }
                    }
                    String noticiahtml = dochtml.getElementsByClass("story-leaf-txt-p").text();
                    if (noticiahtml.isEmpty()) {
                        noticiahtml = dochtml.getElementsByClass("article-modules").text();
                    }
                    docjson.setText(noticiahtml);
                    if (docjson.getText().length() > 0) {
                        FileUtils.writeStringToFile(new File(EXPORT_PATH + docjson.getId() + ".json"), docjson.toJSON("news").toString(), "UTF-8");
                        lvProgressBar.setExtraMessage(docjson.getId() + " has been saved in folder");
                    }
                } catch (Exception e) {
                }
                lvProgressBar.step();

                Thread.sleep(1000);
            }
        } catch (Exception e) {
        }
    }

    //El País (España)     http://ep00.epimg.net/rss/tags/ultimas_noticias.xml
    //extract the information using the http protocol and export it to json format
    public static void elpais() throws InterruptedException {
        String url = "http://ep00.epimg.net/rss/tags/ultimas_noticias.xml";
        try {
            org.jsoup.nodes.Document doc = Jsoup.connect(url).userAgent("Mozilla/5.0").timeout(2000000000).get();
            org.jsoup.select.Elements items = doc.getElementsByTag("item");
            ProgressBar elpaisProgressBar = new ProgressBar("El País (España)", items.size());
            for (int i = 0; i < items.size(); i++) {
                Element noticia = items.get(i);
                Calendar fecha = new GregorianCalendar();
                int año = fecha.get(Calendar.YEAR);
                int mes = fecha.get(Calendar.MONTH);
                int dia = fecha.get(Calendar.DAY_OF_MONTH);
                int hora = fecha.get(Calendar.HOUR_OF_DAY);
                int minuto = fecha.get(Calendar.MINUTE);
                int segundo = fecha.get(Calendar.SECOND);
                SaveJson docjson = new SaveJson();

                String id = "PAI" + dia + (mes + 1) + año + hora + minuto + segundo;
                docjson.setId(id);

                docjson.setDate_harvested(dia + "-" + (mes + 1) + "-" + año + "T" + hora + ":" + minuto + ":" + segundo);

                String title = noticia.getElementsByTag("title").text();
                docjson.setTitle(title);

                String link = noticia.getElementsByTag("link").text();
                docjson.setSource(link);

                String creator = noticia.getElementsByTag("dc:creator").text();
                docjson.setCreator(creator);

                String pubDate = noticia.getElementsByTag("pubDate").text();
                docjson.setPublication_date(pubDate);

                String category = noticia.getElementsByTag("category").text();
                docjson.setCategory(category);

                org.jsoup.nodes.Document dochtml = Jsoup.connect(link).userAgent("Mozilla/5.0").timeout(2000000000).get();
                org.jsoup.select.Elements meta = dochtml.getElementsByTag("meta");
                for (int j = 0; j < meta.size(); j++) {
                    Element name = meta.get(j);
                    String nam = name.attr("name");
                    if (nam.equals("Keywords")) {
                        String val = name.attr("content");
                        docjson.setKeywords(val);
                        break;
                    }
                }
                String noticiahtml = dochtml.getElementsByClass("articulo-cuerpo").text();
                if (noticiahtml.isEmpty()) {
                    noticiahtml = dochtml.getElementsByClass("articulo-texto").text();
                }

                docjson.setText(noticiahtml);
                if (docjson.getText().length() > 0) {
                    FileUtils.writeStringToFile(new File(EXPORT_PATH + docjson.getId() + ".json"), docjson.toJSON("News").toString(), "UTF-8");
                    elpaisProgressBar.setExtraMessage(docjson.getId() + " has been saved in folder");
                }
                elpaisProgressBar.step();

                Thread.sleep(1000);
            }
        } catch (IOException ex) {
            Logger.getLogger(NewsHarvester.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    //Reforma (México)    https://www.reforma.com
    //extract the information using the http protocol and export it to json format
    public static void reforma() throws InterruptedException {
        String url = "https://www.reforma.com/rss/nacional.xml";
        try {
            org.jsoup.nodes.Document doc = Jsoup.connect(url).userAgent("Mozilla/5.0").timeout(2000000000).get();
            org.jsoup.select.Elements items = doc.getElementsByTag("item");
            ProgressBar reformaProgressBar = new ProgressBar("Reforma (México)", items.size());
            for (int i = 0; i < items.size(); i++) {
                Element noticia = items.get(i);
                Calendar fecha = new GregorianCalendar();
                int año = fecha.get(Calendar.YEAR);
                int mes = fecha.get(Calendar.MONTH);
                int dia = fecha.get(Calendar.DAY_OF_MONTH);
                int hora = fecha.get(Calendar.HOUR_OF_DAY);
                int minuto = fecha.get(Calendar.MINUTE);
                int segundo = fecha.get(Calendar.SECOND);
                SaveJson docjson = new SaveJson();

                String id = "REF" + dia + (mes + 1) + año + hora + minuto + segundo;
                docjson.setId(id);

                docjson.setDate_harvested(dia + "-" + (mes + 1) + "-" + año + "T" + hora + ":" + minuto + ":" + segundo);

                String title = noticia.getElementsByTag("title").text();
                docjson.setTitle(title);

                String link = noticia.getElementsByTag("link").text();
                docjson.setSource(link);

                String creator = noticia.getElementsByTag("author").text();
                docjson.setCreator(creator);

                String pubDate = noticia.getElementsByTag("pubDate").text();
                docjson.setPublication_date(pubDate);

                String category = noticia.getElementsByTag("IdCategoria").text();
                docjson.setCategory(category);

                String description = noticia.getElementsByTag("description").text();
                docjson.setText(description);

                try {

                    link = "https://www.reforma.com/aplicacioneslibre/preacceso/articulo/default.aspx?urlredirect=" + link;
                    org.jsoup.nodes.Document dochtml = Jsoup.connect(link).userAgent("Mozilla/5.0").timeout(2000000000).get();
                    org.jsoup.select.Elements meta = dochtml.getElementsByTag("meta");
                    for (int j = 0; j < meta.size(); j++) {
                        Element name = meta.get(j);
                        String nam = name.attr("name");
                        if (nam.equals("Keywords")) {
                            String val = name.attr("content");
                            docjson.setKeywords(val);
                            break;
                        }
                    }
                } catch (Exception e) {
                }

                if (docjson.getText().length() > 0) {
                    FileUtils.writeStringToFile(new File(EXPORT_PATH + docjson.getId() + ".json"), docjson.toJSON("news").toString(), "UTF-8");
                    reformaProgressBar.setExtraMessage(docjson.getId() + " has been saved in folder");
                }
                reformaProgressBar.step();
                Thread.sleep(1000);
            }
        } catch (IOException ex) {
            Logger.getLogger(NewsHarvester.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    // El Comercio (Perú)    https://elcomercio.pe
    //extract the information using the http protocol and export it to json format
    public static void elcomercio() throws InterruptedException {
        String url = "https://archivo.elcomercio.pe/feed/politica.xml";
        try {
            org.jsoup.nodes.Document doc = Jsoup.connect(url).userAgent("Mozilla/5.0").timeout(2000000000).get();
            org.jsoup.select.Elements items = doc.getElementsByTag("item");
            ProgressBar elcomeProgressBar = new ProgressBar("El Comercio (Perú)", items.size());
            for (int i = 0; i < items.size(); i++) {
                Element noticia = items.get(i);
                Calendar fecha = new GregorianCalendar();
                int año = fecha.get(Calendar.YEAR);
                int mes = fecha.get(Calendar.MONTH);
                int dia = fecha.get(Calendar.DAY_OF_MONTH);
                int hora = fecha.get(Calendar.HOUR_OF_DAY);
                int minuto = fecha.get(Calendar.MINUTE);
                int segundo = fecha.get(Calendar.SECOND);
                SaveJson docjson = new SaveJson();

                String id = "COM" + dia + (mes + 1) + año + hora + minuto + segundo;
                docjson.setId(id);

                docjson.setDate_harvested(dia + "-" + (mes + 1) + "-" + año + "T" + hora + ":" + minuto + ":" + segundo);

                String title = noticia.getElementsByTag("title").text();
                docjson.setTitle(title);

                String link = noticia.getElementsByTag("link").text();
                docjson.setSource(link);

                String creator = noticia.getElementsByTag("author").text();
                docjson.setCreator(creator);

                String pubDate = noticia.getElementsByTag("pubDate").text();
                docjson.setPublication_date(pubDate);

                String category = noticia.getElementsByTag("IdCategoria").text();
                docjson.setCategory(category);

                String content = noticia.getElementsByTag("content").text();
                Document dochtml = Jsoup.parse(content);
                docjson.setText(dochtml.text());

                if (docjson.getText().length() > 0) {
                    FileUtils.writeStringToFile(new File(EXPORT_PATH + docjson.getId() + ".json"), docjson.toJSON("news").toString(), "UTF-8");
                    elcomeProgressBar.setExtraMessage(docjson.getId() + " has been saved in folder");
                }
                elcomeProgressBar.step();

                Thread.sleep(1000);
            }
        } catch (IOException ex) {
            Logger.getLogger(NewsHarvester.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    // La República (Perú)    https://larepublica.pe
    //extract the information using the http protocol and export it to json format
    public static void larepublica() throws InterruptedException {
        String url = "https://larepublica.pe/noticiasfeed.xml?outputType=rss";
        try {
            org.jsoup.nodes.Document doc = Jsoup.connect(url).userAgent("Mozilla/5.0").timeout(2000000000).get();
            org.jsoup.select.Elements items = doc.getElementsByTag("item");
            ProgressBar larepupProgressBar = new ProgressBar("La República (Perú)", items.size());
            for (int i = 0; i < items.size(); i++) {
                Element noticia = items.get(i);
                Calendar fecha = new GregorianCalendar();
                int año = fecha.get(Calendar.YEAR);
                int mes = fecha.get(Calendar.MONTH);
                int dia = fecha.get(Calendar.DAY_OF_MONTH);
                int hora = fecha.get(Calendar.HOUR_OF_DAY);
                int minuto = fecha.get(Calendar.MINUTE);
                int segundo = fecha.get(Calendar.SECOND);
                SaveJson docjson = new SaveJson();

                String id = "REP" + dia + (mes + 1) + año + hora + minuto + segundo;
                docjson.setId(id);

                docjson.setDate_harvested(dia + "-" + (mes + 1) + "-" + año + "T" + hora + ":" + minuto + ":" + segundo);

                String title = noticia.getElementsByTag("title").text();
                docjson.setTitle(title);

                String link = noticia.getElementsByTag("link").text();
                docjson.setSource(link);

                String creator = noticia.getElementsByTag("author").text();
                docjson.setCreator(creator);

                String pubDate = noticia.getElementsByTag("pubDate").text();
                docjson.setPublication_date(pubDate);

                String category = noticia.getElementsByTag("IdCategoria").text();
                docjson.setCategory(category);

                String content = noticia.getElementsByTag("content").text();
                docjson.setText(content);

                org.jsoup.nodes.Document dochtml = Jsoup.connect(link).userAgent("Mozilla/5.0").timeout(2100000000).get();
                org.jsoup.select.Elements meta = dochtml.getElementsByTag("meta");
                for (int j = 0; j < meta.size(); j++) {
                    Element name = meta.get(j);
                    String nam = name.attr("name");
                    if (nam.equals("Keywords")) {
                        String val = name.attr("content");
                        docjson.setKeywords(val);
                        break;
                    }
                }
                String noticiahtml = dochtml.getElementsByClass("page-internal-content").text();
                docjson.setText(noticiahtml);
                if (docjson.getText().length() > 0) {
                    FileUtils.writeStringToFile(new File(EXPORT_PATH + docjson.getId() + ".json"), docjson.toJSON("news").toString(), "UTF-8");
                    larepupProgressBar.setExtraMessage(docjson.getId() + " has been saved in folder");
                }
                larepupProgressBar.step();

                Thread.sleep(1000);
            }
        } catch (IOException ex) {
            Logger.getLogger(NewsHarvester.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    // El Observador (Uruguay)    https://www.elobservador.com.uy
    //extract the information using the http protocol and export it to json format
    public static void elobservador() throws InterruptedException {
        String url = "https://www.elobservador.com.uy/rss/elobservador.xml";
        try {
            org.jsoup.nodes.Document doc = Jsoup.connect(url).userAgent("Mozilla/5.0").timeout(2000000000).get();
            org.jsoup.select.Elements items = doc.getElementsByTag("item");
            ProgressBar elobspProgressBar = new ProgressBar("El Observador (Uruguay)", items.size());
            for (int i = 0; i < items.size(); i++) {
                Element noticia = items.get(i);
                Calendar fecha = new GregorianCalendar();
                int año = fecha.get(Calendar.YEAR);
                int mes = fecha.get(Calendar.MONTH);
                int dia = fecha.get(Calendar.DAY_OF_MONTH);
                int hora = fecha.get(Calendar.HOUR_OF_DAY);
                int minuto = fecha.get(Calendar.MINUTE);
                int segundo = fecha.get(Calendar.SECOND);
                SaveJson docjson = new SaveJson();

                String id = "OBS" + dia + (mes + 1) + año + hora + minuto + segundo;
                docjson.setId(id);

                docjson.setDate_harvested(dia + "-" + (mes + 1) + "-" + año + "T" + hora + ":" + minuto + ":" + segundo);

                String title = noticia.getElementsByTag("title").text();
                docjson.setTitle(title);

                String link = noticia.getElementsByTag("link").text();
                docjson.setSource(link);

                String creator = noticia.getElementsByTag("author").text();
                docjson.setCreator(creator);

                String pubDate = noticia.getElementsByTag("pubDate").text();
                docjson.setPublication_date(pubDate);

                String category = noticia.getElementsByTag("IdCategoria").text();
                docjson.setCategory(category);

                String content = noticia.getElementsByTag("description").text();
                docjson.setText(content);

                try {

                    //link = "https://www.reforma.com/aplicacioneslibre/preacceso/articulo/default.aspx?urlredirect=" + link;
                    org.jsoup.nodes.Document dochtml = Jsoup.connect(link).userAgent("Mozilla/5.0").timeout(2000000000).get();
                    org.jsoup.select.Elements meta = dochtml.getElementsByTag("meta");
                    for (int j = 0; j < meta.size(); j++) {
                        Element name = meta.get(j);
                        String nam = name.attr("name");
                        if (nam.equals("Keywords")) {
                            String val = name.attr("content");
                            docjson.setKeywords(val);
                            break;
                        }
                    }
                    String noticiahtml = dochtml.getElementsByClass("cuerpo intro_ mb-3").text();
                    docjson.setText(noticiahtml);
                    //System.out.println("noticiahtml = " + noticiahtml);
                } catch (Exception e) {
                }
                if (docjson.getText().length() > 0) {
                    FileUtils.writeStringToFile(new File(EXPORT_PATH + docjson.getId() + ".json"), docjson.toJSON("news").toString(), "UTF-8");
                    elobspProgressBar.setExtraMessage(docjson.getId() + " has been saved in folder");
                }
                elobspProgressBar.step();

                Thread.sleep(1000);
            }
        } catch (IOException ex) {
            Logger.getLogger(NewsHarvester.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // El tiempo (Colombia)    https://www.eltiempo.com/
    //extract the information using the http protocol and export it to json format
    public static void eltiempo() throws InterruptedException {
        String url = "https://www.eltiempo.com/rss/colombia.xml";
        try {
            org.jsoup.nodes.Document doc = Jsoup.connect(url).userAgent("Mozilla/5.0").timeout(2000000000).get();
            org.jsoup.select.Elements items = doc.getElementsByTag("item");
            ProgressBar eltiempoProgressBar = new ProgressBar("El tiempo (Colombia)", items.size());
            for (int i = 0; i < items.size(); i++) {
                Element noticia = items.get(i);
                Calendar fecha = new GregorianCalendar();
                int año = fecha.get(Calendar.YEAR);
                int mes = fecha.get(Calendar.MONTH);
                int dia = fecha.get(Calendar.DAY_OF_MONTH);
                int hora = fecha.get(Calendar.HOUR_OF_DAY);
                int minuto = fecha.get(Calendar.MINUTE);
                int segundo = fecha.get(Calendar.SECOND);
                SaveJson docjson = new SaveJson();

                String id = "TIE" + dia + (mes + 1) + año + hora + minuto + segundo;
                docjson.setId(id);

                docjson.setDate_harvested(dia + "-" + (mes + 1) + "-" + año + "T" + hora + ":" + minuto + ":" + segundo);

                String title = noticia.getElementsByTag("title").text();
                docjson.setTitle(title);

                String link = noticia.getElementsByTag("link").text();
                docjson.setSource(link);

                String creator = noticia.getElementsByTag("author").text();
                docjson.setCreator(creator);

                String pubDate = noticia.getElementsByTag("pubDate").text();
                docjson.setPublication_date(pubDate);

                String category = noticia.getElementsByTag("IdCategoria").text();
                docjson.setCategory(category);

                String content = noticia.getElementsByTag("description").text();
                docjson.setText(content);
                try {

                    //link = "https://www.reforma.com/aplicacioneslibre/preacceso/articulo/default.aspx?urlredirect=" + link;
                    org.jsoup.nodes.Document dochtml = Jsoup.connect(link).userAgent("Mozilla/5.0").timeout(2000000000).get();
                    org.jsoup.select.Elements meta = dochtml.getElementsByTag("meta");
                    for (int j = 0; j < meta.size(); j++) {
                        Element name = meta.get(j);
                        String nam = name.attr("name");
                        if (nam.equals("Keywords")) {
                            String val = name.attr("content");
                            docjson.setKeywords(val);
                            break;
                        }
                    }
                    String noticiahtml = dochtml.getElementsByClass("contenido").text();
                    docjson.setText(noticiahtml);
                    //System.out.println("noticiahtml = " + noticiahtml);
                } catch (Exception e) {
                }
                if (docjson.getText().length() > 0) {
                    FileUtils.writeStringToFile(new File(EXPORT_PATH + docjson.getId() + ".json"), docjson.toJSON("news").toString(), "UTF-8");
                    eltiempoProgressBar.setExtraMessage(docjson.getId() + " has been saved in folder");
                }
                eltiempoProgressBar.step();
                Thread.sleep(1000);
            }
        } catch (IOException ex) {
            Logger.getLogger(NewsHarvester.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    // Listín Diario (R. Dominicana)    https://listindiario.com
    //extract the information using the http protocol and export it to json format
    public static void listindiario() throws InterruptedException {
        String url = "https://listindiario.com/rss/portada/";
        try {
            org.jsoup.nodes.Document doc = Jsoup.connect(url).userAgent("Mozilla/5.0").timeout(2000000000).get();
            org.jsoup.select.Elements items = doc.getElementsByTag("item");
            ProgressBar listpProgressBar = new ProgressBar("Listín Diario (R. Dominicana)", items.size());
            for (int i = 0; i < items.size(); i++) {
                Element noticia = items.get(i);
                Calendar fecha = new GregorianCalendar();
                int año = fecha.get(Calendar.YEAR);
                int mes = fecha.get(Calendar.MONTH);
                int dia = fecha.get(Calendar.DAY_OF_MONTH);
                int hora = fecha.get(Calendar.HOUR_OF_DAY);
                int minuto = fecha.get(Calendar.MINUTE);
                int segundo = fecha.get(Calendar.SECOND);
                SaveJson docjson = new SaveJson();

                String id = "LIS" + dia + (mes + 1) + año + hora + minuto + segundo;
                docjson.setId(id);

                docjson.setDate_harvested(dia + "-" + (mes + 1) + "-" + año + "T" + hora + ":" + minuto + ":" + segundo);

                String title = noticia.getElementsByTag("title").text();
                docjson.setTitle(title);

                String link = noticia.getElementsByTag("link").text();
                docjson.setSource(link);

                String creator = noticia.getElementsByTag("author").text();
                docjson.setCreator(creator);

                String pubDate = noticia.getElementsByTag("pubDate").text();
                docjson.setPublication_date(pubDate);

                String category = noticia.getElementsByTag("IdCategoria").text();
                docjson.setCategory(category);

                String content = noticia.getElementsByTag("description").text();
                docjson.setText(content);
                try {
                    org.jsoup.nodes.Document dochtml = Jsoup.connect(link).userAgent("Mozilla/5.0").timeout(2000000000).get();
                    org.jsoup.select.Elements meta = dochtml.getElementsByTag("meta");
                    for (int j = 0; j < meta.size(); j++) {
                        Element name = meta.get(j);
                        String nam = name.attr("name");
                        if (nam.equals("Keywords")) {
                            String val = name.attr("content");
                            docjson.setKeywords(val);
                            break;
                        }
                    }
                    String noticiahtml = dochtml.getElementById("ArticleBody").text();
                    docjson.setText(noticiahtml);
                    //System.out.println("noticiahtml = " + noticiahtml);
                    if (docjson.getText().length() > 0) {
                        FileUtils.writeStringToFile(new File(EXPORT_PATH + docjson.getId() + ".json"), docjson.toJSON("news").toString(), "UTF-8");
                        listpProgressBar.setExtraMessage(docjson.getId() + " has been saved in folder");
                    }
                    listpProgressBar.step();
                    Thread.sleep(1000);
                } catch (Exception e) {
                }

            }
        } catch (IOException ex) {
            Logger.getLogger(NewsHarvester.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    // Clarín (Argentina)    https://www.clarin.com
    //extract the information using the http protocol and export it to json format
    public static void clarin() throws InterruptedException {
        String url = "https://www.clarin.com/rss/lo-ultimo/";
        try {
            org.jsoup.nodes.Document doc = Jsoup.connect(url).userAgent("Mozilla/5.0").timeout(2000000000).get();
            org.jsoup.select.Elements items = doc.getElementsByTag("item");
            ProgressBar clarinProgressBar = new ProgressBar("Clarín (Argentina)", items.size());
            for (int i = 0; i < items.size(); i++) {
                Element noticia = items.get(i);
                Calendar fecha = new GregorianCalendar();
                int año = fecha.get(Calendar.YEAR);
                int mes = fecha.get(Calendar.MONTH);
                int dia = fecha.get(Calendar.DAY_OF_MONTH);
                int hora = fecha.get(Calendar.HOUR_OF_DAY);
                int minuto = fecha.get(Calendar.MINUTE);
                int segundo = fecha.get(Calendar.SECOND);
                SaveJson docjson = new SaveJson();

                String id = "CLA" + dia + (mes + 1) + año + hora + minuto + segundo;
                docjson.setId(id);

                docjson.setDate_harvested(dia + "-" + (mes + 1) + "-" + año + "T" + hora + ":" + minuto + ":" + segundo);

                String title = noticia.getElementsByTag("title").text();
                docjson.setTitle(title);

                String link = noticia.getElementsByTag("link").text();
                docjson.setSource(link);

                String creator = noticia.getElementsByTag("author").text();
                docjson.setCreator(creator);

                String pubDate = noticia.getElementsByTag("pubDate").text();
                docjson.setPublication_date(pubDate);

                String category = noticia.getElementsByTag("IdCategoria").text();
                docjson.setCategory(category);

                String content = noticia.getElementsByTag("description").text();
                docjson.setText(content);

                try {

                    org.jsoup.nodes.Document dochtml = Jsoup.connect(link).userAgent("Mozilla/5.0").timeout(2000000000).get();
                    org.jsoup.select.Elements meta = dochtml.getElementsByTag("meta");
                    for (int j = 0; j < meta.size(); j++) {
                        Element name = meta.get(j);
                        String nam = name.attr("name");
                        if (nam.equals("Keywords")) {
                            String val = name.attr("content");
                            docjson.setKeywords(val);
                            break;
                        }
                    }
                    String noticiahtml = dochtml.getElementsByClass("body-nota").text();
                    docjson.setText(noticiahtml);
                    //System.out.println("noticiahtml = " + noticiahtml);
                } catch (Exception e) {
                }

                if (docjson.getText().length() > 0) {
                    FileUtils.writeStringToFile(new File(EXPORT_PATH + docjson.getId() + ".json"), docjson.toJSON("news").toString(), "UTF-8");
                    clarinProgressBar.setExtraMessage(docjson.getId() + " has been saved in folder");
                }
                clarinProgressBar.step();
                Thread.sleep(1000);
            }
        } catch (IOException ex) {
            Logger.getLogger(NewsHarvester.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

// La Nación (Argentina)    https://www.lanacion.com.ar
    //extract the information using the http protocol and export it to json format
    public static void lanacion() throws InterruptedException {
        String url = "http://contenidos.lanacion.com.ar/herramientas/rss-origen=2";
        try {
            org.jsoup.nodes.Document doc = Jsoup.connect(url).userAgent("Mozilla/5.0").timeout(2000000000).get();
            org.jsoup.select.Elements items = doc.getElementsByTag("entry");
            ProgressBar lanacionProgressBar = new ProgressBar("La Nación (Argentina)", items.size());
            for (int i = 0; i < items.size(); i++) {
                Element noticia = items.get(i);
                Calendar fecha = new GregorianCalendar();
                int año = fecha.get(Calendar.YEAR);
                int mes = fecha.get(Calendar.MONTH);
                int dia = fecha.get(Calendar.DAY_OF_MONTH);
                int hora = fecha.get(Calendar.HOUR_OF_DAY);
                int minuto = fecha.get(Calendar.MINUTE);
                int segundo = fecha.get(Calendar.SECOND);
                SaveJson docjson = new SaveJson();

                String id = "NAC" + dia + (mes + 1) + año + hora + minuto + segundo;
                docjson.setId(id);

                docjson.setDate_harvested(dia + "-" + (mes + 1) + "-" + año + "T" + hora + ":" + minuto + ":" + segundo);

                String title = noticia.getElementsByTag("title").text();
                docjson.setTitle(title);

                org.jsoup.select.Elements link = noticia.getElementsByTag("link");
                String links = link.toString().replaceAll("<link href=\"", "").replaceAll("\" />", "");
                docjson.setSource(links);

                String creator = noticia.getElementsByTag("author").text();
                docjson.setCreator(creator);

                String pubDate = noticia.getElementsByTag("pubDate").text();
                docjson.setPublication_date(pubDate);

                org.jsoup.select.Elements category = noticia.getElementsByTag("category");
                String categorys = category.toString().replaceAll("<category term=\"", "").replaceAll("\" />", "");
                docjson.setCategory(categorys);

                String content = noticia.getElementsByTag("description").text();
                docjson.setText(content);

                try {

                    org.jsoup.nodes.Document dochtml = Jsoup.connect(links).userAgent("Mozilla/5.0").timeout(2000000000).get();
                    org.jsoup.select.Elements meta = dochtml.getElementsByTag("meta");
                    for (int j = 0; j < meta.size(); j++) {
                        Element name = meta.get(j);
                        String nam = name.attr("name");
                        if (nam.equals("Keywords")) {
                            String val = name.attr("content");
                            docjson.setKeywords(val);
                            break;
                        }
                    }
                    String noticiahtml = dochtml.getElementById("cuerpo").text();
                    docjson.setText(noticiahtml);

                } catch (Exception e) {
                }
                if (docjson.getText().length() > 0) {
                    FileUtils.writeStringToFile(new File(EXPORT_PATH + docjson.getId() + ".json"), docjson.toJSON("news").toString(), "UTF-8");
                    lanacionProgressBar.setExtraMessage(docjson.getId() + " has been saved in folder");
                }
                lanacionProgressBar.step();
                Thread.sleep(1000);
            }
        } catch (IOException ex) {
            Logger.getLogger(NewsHarvester.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    //Emol (Chile)    https://www.emol.com
    //extract the information using the http protocol and export it to json format
    public static void Emol() throws InterruptedException {
        String url = "https://www.emol.com";
        ArrayList<String> news = new ArrayList<String>();
        try {
            org.jsoup.nodes.Document doc = Jsoup.connect(url).userAgent("Mozilla/5.0").timeout(2000000000).get();
            Elements html = doc.getElementsByTag("a");
            for (int i = 0; i < html.size(); i++) {
                Element htmlhref = html.get(i);
                String href = htmlhref.attr("href").toString();
                if (href.contains("/noticias/Nacional/")) {
                    if (href.contains("https://www.emol.com/noticias/")) {
                    } else {
                        href = "https://www.emol.com" + href;
                    }
                    news.add(href);
                }
            }
            ProgressBar br = new ProgressBar("Emol (Chile)", news.size());
            for (int x = 0; x < news.size(); x++) {
                String href = news.get(x);
                Calendar fecha = new GregorianCalendar();
                int año = fecha.get(Calendar.YEAR);
                int mes = fecha.get(Calendar.MONTH);
                int dia = fecha.get(Calendar.DAY_OF_MONTH);
                int hora = fecha.get(Calendar.HOUR_OF_DAY);
                int minuto = fecha.get(Calendar.MINUTE);
                int segundo = fecha.get(Calendar.SECOND);
                SaveJson docjson = new SaveJson();
                docjson.setDate_harvested(dia + "-" + (mes + 1) + "-" + año + "T" + hora + ":" + minuto + ":" + segundo);

                String id = "EMO" + dia + (mes + 1) + año + hora + minuto + segundo;
                docjson.setId(id);

                try {

                    org.jsoup.nodes.Document doc1 = Jsoup.connect(href).userAgent("Mozilla/5.0").timeout(2000000000).get();
                    org.jsoup.select.Elements meta = doc1.getElementsByTag("meta");
                    for (int j = 0; j < meta.size(); j++) {
                        Element name = meta.get(j);
                        String nam = name.attr("name");
                        if (nam.equals("Keywords")) {
                            String val = name.attr("content");
                            docjson.setKeywords(val);
                        }
                        if (nam.equals("title")) {
                            String val = name.attr("content");
                            docjson.setTitle(val);
                        }
                        if (nam.equals("author")) {
                            String val = name.attr("content");
                            docjson.setCreator(val);

                        }
                        if (nam.equals("language")) {
                            String val = name.attr("content");
                            docjson.setLanguage(val);

                        }
                        if (nam.equals("title")) {
                            String val = name.attr("content");
                            docjson.setTitle(val);

                        }

                    }
                    docjson.setSource(href);

                    String content = doc1.getElementsByClass("EmolText").text();
                    docjson.setText(content);
                    String datenews = doc1.getElementsByClass("emol_fecha").text();
                    docjson.setPublication_date(datenews);

                } catch (Exception e) {
                }
                if (docjson.getText().length() > 0) {
                    FileUtils.writeStringToFile(new File(EXPORT_PATH + docjson.getId() + ".json"), docjson.toJSON("news").toString(), "UTF-8");
                    br.setExtraMessage(docjson.getId() + " has been saved in folder");
                }
                br.step();
                Thread.sleep(1000);
            }
        } catch (IOException ex) {
            Logger.getLogger(NewsHarvester.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //El Universal (México)    https://www.eluniversal.com.mx
    //extract the information using the http protocol and export it to json format
    public static void eluniversal() throws InterruptedException {
        String url = "https://www.eluniversal.com.mx";
        ArrayList<String> news = new ArrayList<String>();
        try {
            org.jsoup.nodes.Document doc = Jsoup.connect(url).userAgent("Mozilla/5.0").timeout(2000000000).get();
            Elements html = doc.getElementsByTag("a");
            for (int i = 0; i < html.size(); i++) {
                Element htmlhref = html.get(i);
                String href = htmlhref.attr("href").toString();
                if (href.contains("https://www.eluniversal.com.mx/mundo")) {
                    news.add(href);
                }
            }
            ProgressBar br = new ProgressBar("El Universal (México)", news.size());
            for (int x = 0; x < news.size(); x++) {
                String href = news.get(x);
                SaveJson docjson = new SaveJson();

                Calendar fecha = new GregorianCalendar();
                int año = fecha.get(Calendar.YEAR);
                int mes = fecha.get(Calendar.MONTH);
                int dia = fecha.get(Calendar.DAY_OF_MONTH);
                int hora = fecha.get(Calendar.HOUR_OF_DAY);
                int minuto = fecha.get(Calendar.MINUTE);
                int segundo = fecha.get(Calendar.SECOND);
                docjson.setDate_harvested(dia + "-" + (mes + 1) + "-" + año + "T" + hora + ":" + minuto + ":" + segundo);

                String id = "UNI" + dia + (mes + 1) + año + hora + minuto + segundo;
                docjson.setId(id);

                try {

                    org.jsoup.nodes.Document doc1 = Jsoup.connect(href).userAgent("Mozilla/5.0").timeout(2000000000).get();
                    org.jsoup.select.Elements meta = doc1.getElementsByTag("meta");
                    for (int j = 0; j < meta.size(); j++) {
                        Element name = meta.get(j);
                        String nam = name.attr("name");
                        if (nam.equals("keywords")) {
                            String val = name.attr("content");
                            docjson.setKeywords(val);
                        }
                        if (nam.equals("title")) {
                            String val = name.attr("content");
                            docjson.setTitle(val);
                        }
                        if (nam.equals("author")) {
                            String val = name.attr("content");
                            docjson.setCreator(val);
                        }
                        if (nam.equals("language")) {
                            String val = name.attr("content");
                            docjson.setLanguage(val);
                        }
                        if (nam.equals("title")) {
                            String val = name.attr("content");
                            docjson.setTitle(val);
                        }

                    }
                    docjson.setSource(href);

                    String title = doc1.getElementsByClass("Encabezado-Articulo").text();
                    docjson.setTitle(title);

                    String content = doc1.getElementsByClass("field-name-body").text();
                    docjson.setText(content);

                    String datenews = doc1.getElementsByClass("ce12-DatosArticulo_ElementoFecha").text();
                    docjson.setPublication_date(datenews);

                } catch (Exception e) {
                }
                if (docjson.getText().length() > 0) {
                    FileUtils.writeStringToFile(new File(EXPORT_PATH + docjson.getId() + ".json"), docjson.toJSON("news").toString(), "UTF-8");
                    br.setExtraMessage(docjson.getId() + " has been saved in folder");
                }
                br.step();
                Thread.sleep(1000);
            }
        } catch (IOException ex) {
            Logger.getLogger(NewsHarvester.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    //La Tercera (Chile)    https://www.latercera.com
    //extract the information using the http protocol and export it to json format
    public static void latercera() throws InterruptedException {
        String url = "https://www.latercera.com";
        ArrayList<String> news = new ArrayList<String>();
        try {
            org.jsoup.nodes.Document doc = Jsoup.connect(url).userAgent("Mozilla/5.0").timeout(2000000000).get();
            Elements html = doc.getElementsByClass("headline");
            for (int i = 0; i < html.size(); i++) {
                Element htmlhref = html.get(i);
                Elements href = htmlhref.getElementsByTag("a");
                for (int j = 0; j < href.size(); j++) {
                    Element htmlhref2 = href.get(j);
                    String href2 = htmlhref2.attr("href").toString();
                    if (href2.contains("/noticia/")) {
                        href2 = "https://www.latercera.com" + href2;
                        news.add(href2);
                    }
                }
            }
            ProgressBar br = new ProgressBar("La Tercera (Chile)", news.size());
            for (int m = 0; m < news.size(); m++) {
                String href2 = news.get(m);
                SaveJson docjson = new SaveJson();

                Calendar fecha = new GregorianCalendar();
                int año = fecha.get(Calendar.YEAR);
                int mes = fecha.get(Calendar.MONTH);
                int dia = fecha.get(Calendar.DAY_OF_MONTH);
                int hora = fecha.get(Calendar.HOUR_OF_DAY);
                int minuto = fecha.get(Calendar.MINUTE);
                int segundo = fecha.get(Calendar.SECOND);
                docjson.setDate_harvested(dia + "-" + (mes + 1) + "-" + año + "T" + hora + ":" + minuto + ":" + segundo);

                String id = "TER" + dia + (mes + 1) + año + hora + minuto + segundo;
                docjson.setId(id);

                try {
                    org.jsoup.nodes.Document doc1 = Jsoup.connect(href2).userAgent("Mozilla/5.0").timeout(2000000000).get();
                    org.jsoup.select.Elements meta = doc1.getElementsByTag("meta");
                    for (int x = 0; x < meta.size(); x++) {
                        Element name = meta.get(x);
                        String nam = name.attr("property");
                        if (nam.equals("article:tag")) {
                            String val = name.attr("content");
                            docjson.setKeywords(val);
                        }

                        if (nam.equals("article:section")) {
                            String val = name.attr("content");
                            docjson.setCategory(val);
                        }

                        if (nam.equals("article:published_time")) {
                            String val = name.attr("content");
                            docjson.setPublication_date(val);
                        }

                        if (nam.equals("og:locale")) {
                            String val = name.attr("content");
                            docjson.setLanguage(val);
                        }

                        if (nam.equals("og:title")) {
                            String val = name.attr("content");
                            docjson.setTitle(val);
                        }
                    }
                    docjson.setSource(href2);

                    String autor = doc1.getElementsByClass("name").text();
                    docjson.setCreator(autor);

                    String content = doc1.getElementsByClass("single-content").text();
                    docjson.setText(content);

                    if (docjson.getText().length() > 0) {
                        FileUtils.writeStringToFile(new File(EXPORT_PATH + docjson.getId() + ".json"), docjson.toJSON("news").toString(), "UTF-8");
                        br.setExtraMessage(docjson.getId() + " has been saved in folder");
                    }
                } catch (Exception e) {
                }
                br.step();

                Thread.sleep(1000);
            }

        } catch (IOException ex) {
            Logger.getLogger(NewsHarvester.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //El País (Uruguay)    https://www.elpais.com.uy
    //extract the information using the http protocol and export it to json format
    public static void elpaisuy() throws InterruptedException {
        String url = "https://www.elpais.com.uy";
        ArrayList<String> news = new ArrayList<String>();
        try {
            org.jsoup.nodes.Document doc = Jsoup.connect(url).userAgent("Mozilla/5.0").timeout(2000000000).get();
            Elements html = doc.getElementsByClass("page-link");
            for (int i = 0; i < html.size(); i++) {
                Element htmlhref = html.get(i);
                String href = htmlhref.attr("href").toString();
                if (href.startsWith("/")) {
                    href = "https://www.elpais.com.uy" + href;
                    news.add(href);
                }
            }

            ProgressBar br = new ProgressBar("El País (Uruguay)", news.size());
            for (int m = 0; m < news.size(); m++) {
                String href = news.get(m);

                SaveJson docjson = new SaveJson();

                Calendar fecha = new GregorianCalendar();
                int año = fecha.get(Calendar.YEAR);
                int mes = fecha.get(Calendar.MONTH);
                int dia = fecha.get(Calendar.DAY_OF_MONTH);
                int hora = fecha.get(Calendar.HOUR_OF_DAY);
                int minuto = fecha.get(Calendar.MINUTE);
                int segundo = fecha.get(Calendar.SECOND);
                docjson.setDate_harvested(dia + "-" + (mes + 1) + "-" + año + "T" + hora + ":" + minuto + ":" + segundo);

                String id = "PUY" + dia + (mes + 1) + año + hora + minuto + segundo;
                docjson.setId(id);

                try {

                    org.jsoup.nodes.Document doc1 = Jsoup.connect(href).userAgent("Mozilla/5.0").timeout(2000000000).get();
                    org.jsoup.select.Elements meta = doc1.getElementsByTag("meta");
                    for (int j = 0; j < meta.size(); j++) {
                        Element name = meta.get(j);
                        String nam = name.attr("name");
                        if (nam.equals("author")) {
                            String val = name.attr("content");
                            docjson.setCreator(val);

                        }
                        if (nam.equals("cXenseParse:section")) {
                            String val = name.attr("content");
                            docjson.setCategory(val);

                        }
                        if (nam.equals("language")) {
                            String val = name.attr("content");
                            docjson.setLanguage(val);

                        }
                        if (nam.equals("cXenseParse:recs:publishtime")) {
                            String val = name.attr("content");
                            docjson.setPublication_date(val);
                        }

                    }
                    docjson.setSource(href);

                    String title = doc1.getElementsByClass("title").text();
                    docjson.setTitle(title);

                    String content = doc1.getElementsByClass("content-modules").text();
                    docjson.setText(content);
                    if (docjson.getText().length() > 0) {
                        FileUtils.writeStringToFile(new File(EXPORT_PATH + docjson.getId() + ".json"), docjson.toJSON("news").toString(), "UTF-8");
                        br.setExtraMessage(docjson.getId() + " has been saved in folder");
                    }
                    br.step();
                    Thread.sleep(1000);
                } catch (Exception e) {
                }

            }
        } catch (IOException ex) {
            Logger.getLogger(NewsHarvester.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //El espectador (Colombia)    https://www.elespectador.com
//extract the information using the http protocol and export it to json format
    public static void elespectador() throws InterruptedException {
        String url = "https://www.elespectador.com";
        ArrayList<String> news = new ArrayList<String>();
        try {
            org.jsoup.nodes.Document doc = Jsoup.connect(url).userAgent("Mozilla/5.0").timeout(2000000000).get();
            Elements html = doc.getElementsByClass("Card-title");
            for (int i = 0; i < html.size(); i++) {
                Element htmlhref = html.get(i);
                Elements href = htmlhref.getElementsByTag("a");
                for (int j = 0; j < href.size(); j++) {
                    Element htmlhref2 = href.get(j);
                    String href2 = htmlhref2.attr("href").toString();
                    if (href2.contains("/noticias/")) {
                        href2 = "https://www.elespectador.com" + href2;
                        news.add(href2);

                    }
                }
            }

            ProgressBar br = new ProgressBar("El espectador (Colombia)", news.size());
            for (int m = 0; m < news.size(); m++) {
                String href2 = news.get(m);

                SaveJson docjson = new SaveJson();

                Calendar fecha = new GregorianCalendar();
                int año = fecha.get(Calendar.YEAR);
                int mes = fecha.get(Calendar.MONTH);
                int dia = fecha.get(Calendar.DAY_OF_MONTH);
                int hora = fecha.get(Calendar.HOUR_OF_DAY);
                int minuto = fecha.get(Calendar.MINUTE);
                int segundo = fecha.get(Calendar.SECOND);
                docjson.setDate_harvested(dia + "-" + (mes + 1) + "-" + año + "T" + hora + ":" + minuto + ":" + segundo);

                String id = "ESP" + dia + (mes + 1) + año + hora + minuto + segundo;
                docjson.setId(id);

                try {

                    org.jsoup.nodes.Document doc1 = Jsoup.connect(href2).userAgent("Mozilla/5.0").timeout(2000000000).get();
                    org.jsoup.select.Elements meta = doc1.getElementsByTag("meta");
                    for (int x = 0; x < meta.size(); x++) {
                        Element name = meta.get(x);
                        String nam = name.attr("name");
                        if (nam.equals("keywords")) {
                            String val = name.attr("content");
                            docjson.setKeywords(val);
                        }
                        if (nam.equals("language")) {
                            String val = name.attr("content");
                            docjson.setLanguage(val);

                        }

                        if (nam.equals("author")) {
                            String val = name.attr("content");
                            docjson.setCreator(val);

                        }

                    }
                    docjson.setSource(href2);

                    String title = doc1.getElementsByClass("Article-Title").text();
                    docjson.setTitle(title);

                    String time = doc1.getElementsByClass("Article-Time").text();
                    docjson.setPublication_date(time);

                    String autor = doc1.getElementsByClass("Article-Author").text();
                    docjson.setCreator(autor);

                    String content = doc1.getElementsByClass("Article-Content").text();
                    docjson.setText(content);

                } catch (Exception e) {
                }

                if (docjson.getText().length() > 0) {
                    FileUtils.writeStringToFile(new File(EXPORT_PATH + docjson.getId() + ".json"), docjson.toJSON("news").toString(), "UTF-8");
                    br.setExtraMessage(docjson.getId() + " has been saved in folder");
                }
                br.step();
                Thread.sleep(1000);
            }

        } catch (IOException ex) {
            Logger.getLogger(NewsHarvester.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //La Nación (Argentina)    https://www.lanacion.com.ar
//extract the information using the http protocol and export it to json format
    public static void lanacionarg() throws InterruptedException {
        String url = "https://www.lanacion.com.ar";
        ArrayList<String> news = new ArrayList<String>();
        try {
            org.jsoup.nodes.Document doc = Jsoup.connect(url).userAgent("Mozilla/5.0").timeout(2000000000).get();
            Elements html = doc.getElementsByClass("com-title");
            for (int i = 0; i < html.size(); i++) {
                Element htmlhref = html.get(i);
                Elements href = htmlhref.getElementsByTag("a");
                for (int j = 0; j < href.size(); j++) {
                    Element htmlhref2 = href.get(j);
                    String href2 = htmlhref2.attr("href").toString();
                    if (href2.startsWith("/")) {
                        href2 = "https://www.lanacion.com.ar" + href2;
                        news.add(href2);

                    }
                }
            }
            ProgressBar br = new ProgressBar("La Nación (Argentina)", news.size());
            for (int m = 0; m < news.size(); m++) {
                String href2 = news.get(m);

                SaveJson docjson = new SaveJson();

                Calendar fecha = new GregorianCalendar();
                int año = fecha.get(Calendar.YEAR);
                int mes = fecha.get(Calendar.MONTH);
                int dia = fecha.get(Calendar.DAY_OF_MONTH);
                int hora = fecha.get(Calendar.HOUR_OF_DAY);
                int minuto = fecha.get(Calendar.MINUTE);
                int segundo = fecha.get(Calendar.SECOND);
                docjson.setDate_harvested(dia + "-" + (mes + 1) + "-" + año + "T" + hora + ":" + minuto + ":" + segundo);

                String id = "NAC" + dia + (mes + 1) + año + hora + minuto + segundo;
                docjson.setId(id);

                try {

                    org.jsoup.nodes.Document doc1 = Jsoup.connect(href2).userAgent("Mozilla/5.0").timeout(2000000000).get();
                    org.jsoup.select.Elements meta = doc1.getElementsByTag("meta");
                    for (int x = 0; x < meta.size(); x++) {
                        Element name = meta.get(x);
                        String nam = name.attr("name");
                        if (nam.equals("keywords")) {
                            String val = name.attr("content");
                            docjson.setKeywords(val);
                        }

                        if (nam.equals("title")) {
                            String val = name.attr("content");
                            docjson.setTitle(val);
                            docjson.setLanguage("Spanish");

                        }
                    }
                    docjson.setSource(href2);

                    String title = doc1.getElementsByClass("titulo").text();
                    docjson.setTitle(title);

                    String time = doc1.getElementsByClass("fecha").text();
                    docjson.setPublication_date(time);

                    String content = doc1.getElementById("cuerpo").text();
                    docjson.setText(content);
                    if (docjson.getText().length() > 0) {
                        FileUtils.writeStringToFile(new File(EXPORT_PATH + docjson.getId() + ".json"), docjson.toJSON("news").toString(), "UTF-8");
                        br.setExtraMessage(docjson.getId() + " has been saved in folder");
                    }
                    br.step();
                } catch (Exception e) {
                }

                Thread.sleep(1000);
            }
        } catch (IOException ex) {

        }
    }

    //Las últimas noticias (Venezuela)    https://ultimasnoticias.com.ve
//extract the information using the http protocol and export it to json format
    public static void ultimasnoticiasve() throws InterruptedException {
        String url = "https://ultimasnoticias.com.ve";
        ArrayList<String> news = new ArrayList<String>();
        try {
            org.jsoup.nodes.Document doc = Jsoup.connect(url).userAgent("Mozilla/5.0").timeout(2000000000).get();
            Elements html = doc.getElementsByClass("jet-smart-listing__post-title");
            for (int i = 0; i < html.size(); i++) {
                Element htmlhref = html.get(i);
                Elements href = htmlhref.getElementsByTag("a");
                for (int j = 0; j < href.size(); j++) {
                    Element htmlhref2 = href.get(j);
                    String href2 = htmlhref2.attr("href").toString();
                    if (href2.startsWith("https://ultimasnoticias.com.ve/")) {
                        news.add(href2);

                    }
                }
            }
            ProgressBar br = new ProgressBar("Las últimas noticias (Venezuela)", news.size());
            for (int m = 0; m < news.size(); m++) {
                String href2 = news.get(m);

                SaveJson docjson = new SaveJson();

                Calendar fecha = new GregorianCalendar();
                int año = fecha.get(Calendar.YEAR);
                int mes = fecha.get(Calendar.MONTH);
                int dia = fecha.get(Calendar.DAY_OF_MONTH);
                int hora = fecha.get(Calendar.HOUR_OF_DAY);
                int minuto = fecha.get(Calendar.MINUTE);
                int segundo = fecha.get(Calendar.SECOND);
                docjson.setDate_harvested(dia + "-" + (mes + 1) + "-" + año + "T" + hora + ":" + minuto + ":" + segundo);

                String id = "UNT" + dia + (mes + 1) + año + hora + minuto + segundo;
                docjson.setId(id);

                try {

                    org.jsoup.nodes.Document doc1 = Jsoup.connect(href2).userAgent("Mozilla/5.0").timeout(2000000000).get();
                    org.jsoup.select.Elements meta = doc1.getElementsByTag("meta");
                    for (int x = 0; x < meta.size(); x++) {
                        Element name = meta.get(x);
                        String nam = name.attr("name");
                        if (nam.equals("keywords")) {
                            String val = name.attr("content");
                            docjson.setKeywords(val);
                        }

                    }
                    docjson.setSource(href2);

                    String title = doc1.getElementsByClass("elementor-heading-title").text();
                    docjson.setTitle(title);

                    String time = doc1.getElementsByClass("elementor-post-info__item--type-date").text();
                    docjson.setPublication_date(time);

                    String autor = doc1.getElementsByClass("elementor-author-box__name").text();
                    docjson.setCreator(autor);

                    docjson.setLanguage("Spanish");

                    String content = doc1.getElementsByClass("elementor-widget-container").text();
                    docjson.setText(content);
                    if (docjson.getText().length() > 0) {
                        FileUtils.writeStringToFile(new File(EXPORT_PATH + docjson.getId() + ".json"), docjson.toJSON("news").toString(), "UTF-8");
                        br.setExtraMessage(docjson.getId() + " has been saved in folder");
                    }
                    br.step();

                } catch (Exception e) {
                }

                Thread.sleep(1000);
            }
        } catch (IOException ex) {

        }
    }

    //El Universal (Venezuela)    https://www.eluniversal.com
//extract the information using the http protocol and export it to json format
    public static void eluniversalven() throws InterruptedException {
        String url = "https://www.eluniversal.com";
        ArrayList<String> hrefarray = new ArrayList<String>();
        try {
            org.jsoup.nodes.Document doc = Jsoup.connect(url).userAgent("Mozilla/5.0").timeout(2000000000).get();
            Elements html = doc.getElementsByTag("a");
            for (int i = 0; i < html.size(); i++) {
                Element htmlhref = html.get(i);
                String href = htmlhref.attr("href").toString();
                if (href.contains("http://www.eluniversal.com/")) {
                    hrefarray.add(href);
                }
            }
            ProgressBar br = new ProgressBar("El Universal (Venezuela) ", hrefarray.size());
            for (int x = 0; x < hrefarray.size(); x++) {
                try {
                    String href = hrefarray.get(x);
                    SaveJson docjson = new SaveJson();
                    Calendar fecha = new GregorianCalendar();
                    int año = fecha.get(Calendar.YEAR);
                    int mes = fecha.get(Calendar.MONTH);
                    int dia = fecha.get(Calendar.DAY_OF_MONTH);
                    int hora = fecha.get(Calendar.HOUR_OF_DAY);
                    int minuto = fecha.get(Calendar.MINUTE);
                    int segundo = fecha.get(Calendar.SECOND);
                    docjson.setDate_harvested(dia + "-" + (mes + 1) + "-" + año + "T" + hora + ":" + minuto + ":" + segundo);

                    String id = "UNV" + dia + (mes + 1) + año + hora + minuto + segundo;
                    docjson.setId(id);

                    try {

                        org.jsoup.nodes.Document doc1 = Jsoup.connect(href).userAgent("Mozilla/5.0").timeout(2000000000).get();
                        org.jsoup.select.Elements meta = doc1.getElementsByTag("meta");
                        for (int j = 0; j < meta.size(); j++) {
                            Element name = meta.get(j);
                            String nam = name.attr("property");
                            if (nam.equals("article:published_time")) {
                                String val = name.attr("content");
                                docjson.setPublication_date(val);
                            }
                        }
                        docjson.setSource(href);

                        docjson.setLanguage("Spanish");

                        String title = doc1.getElementById("titulo").text();
                        docjson.setTitle(title);

                        String content = doc1.getElementsByClass("note-text").text();
                        docjson.setText(content);

                    } catch (Exception e) {
                    }

                    if (docjson.getText().length() > 0) {
                        FileUtils.writeStringToFile(new File(EXPORT_PATH + docjson.getId() + ".json"), docjson.toJSON("news").toString(), "UTF-8");
                        br.setExtraMessage(docjson.getId() + " has been saved in folder");
                    }
                    br.step();
                    Thread.sleep(1000);
                } catch (Exception e) {
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(NewsHarvester.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
